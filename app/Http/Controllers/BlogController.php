<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Blog;
use App\Events\PembuatBlogCreatedBlogEvent;
use App\Events\UserRegisteredEvent;
use App\Events\EditorBlogReviewBlogEvent;
use App\Events\PublishBlogEvent;

class BlogController extends Controller
{
    public function createBlog(Request $request){
    	$data = [];

    	$userpembuatblog_id = request('userpembuatblog_id');
    	$usereditorblog_id = request('usereditorblog_id');
        $blogCreate = Blog::create([
            'judul' => request('judul'),
            'slug' => request('slug'),
            'publish_status' => request('publish_status'),
            'userpembuatblog_id' => $userpembuatblog_id,
            'usereditorblog_id' => $usereditorblog_id,
        ]);

        $pembuatBlog = User::find($userpembuatblog_id);
        $editorBlog = User::find($usereditorblog_id);

        $data['blog'] = $blogCreate;
        $data['pembuatBlog'] =  $pembuatBlog;
        $data['editorBlog'] = $editorBlog;
        
        // Mail::to($pembuatBlog)->send(new PembuatBlogCreatedBlogMail($pembuatBlog,$blogCreate));
        // Mail::to($editorBlog)->send(new EditorBlogReviewerMail($editorBlog,$blogCreate));
       // event(new UserRegisteredEvent($user));
        
        event(new PembuatBlogCreatedBlogEvent($pembuatBlog,$blogCreate));
        event(new EditorBlogReviewBlogEvent($editorBlog,$blogCreate));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'blog berhasil di buat',
            'data' => $data,
        ], 200);
    }

    public function publishBlog(Request $Request,$idBlog){
        $blog = Blog::where('id',$idBlog)->update(['publish_status' => 1]);
        $data = Blog::find($idBlog);
        $pembuatBlog_id = $data->userpembuatblog_id;
        $pembuatBlog = User::find($pembuatBlog_id);

        event(new PublishBlogEvent($pembuatBlog, $data));
        
        return response()->json([
            'response_code' => '00',
            'response_message' => 'blog berhasil di publish',
            'data' => $data,
        ], 200);

    }
}
