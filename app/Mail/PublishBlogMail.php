<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Blog;

class PublishBlogMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($judulBlog)
    {
          $this->blog = $judulBlog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
          return $this->from('example@example.com')
                ->view('send_email_publishblog')
                ->with([
                        'judul' => $this->blog->judul,
                ]);
    }
}
