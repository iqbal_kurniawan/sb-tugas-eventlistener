<?php

namespace App\Listeners;

use App\Events\PembuatBlogCreatedBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\PembuatBlogCreatedBlogMail;

class SendEmailNotificationPembuatBlogCreatedBlog implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PembuatBlogCreatedBlogEvent  $event
     * @return void
     */
    public function handle(PembuatBlogCreatedBlogEvent $event)
    {
        //
        Mail::to($event->user)->send(new PembuatBlogCreatedBlogMail($event->user,$event->blog));
    }
}
