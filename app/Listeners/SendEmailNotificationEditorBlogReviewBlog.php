<?php

namespace App\Listeners;

use App\Events\EditorBlogReviewBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\EditorBlogReviewerMail;

class SendEmailNotificationEditorBlogReviewBlog implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EditorBlogReviewBlogEvent  $event
     * @return void
     */
    public function handle(EditorBlogReviewBlogEvent $event)
    {
          Mail::to($event->user)->send(new EditorBlogReviewerMail($event->user,$event->blog));
    }
}
