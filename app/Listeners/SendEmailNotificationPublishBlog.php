<?php

namespace App\Listeners;

use App\Events\PublishBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\PublishBlogMail;

class SendEmailNotificationPublishBlog  implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PublishBlogEvent  $event
     * @return void
     */
    public function handle(PublishBlogEvent $event)
    {
        //
          Mail::to($event->user)->send(new PublishBlogMail($event->blog));
    }
}
